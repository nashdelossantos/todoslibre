<?php

namespace Shann\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ExperienceType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array(
                'label'     => 'Job Title',
                'attr'      => array(
                    'class' => 'form-control'
                )
            ))
            ->add('description', TextareaType::class, array(
                'label'     => 'Job Description',
                'attr'      => array(
                    'class' => 'form-control',
                    'rows'  => 13
                )
            ))
            ->add('start', DateType::class, array(
                'widget'    => 'single_text',
                'attr'      => array(
                    'class' => 'form-control'
                )
            ))
            ->add('end', DateType::class, array(
                'widget'    => 'single_text',
                'attr'      => array(
                    'class' => 'form-control'
                )
            ))
            ->add('company', TextType::class, array(
                'attr'      => array(
                    'class' => 'form-control'
                )
            ))
            ->add('contactPerson', TextType::class, array(
                'attr'      => array(
                    'class' => 'form-control'
                )
            ))
            ->add('contactNumber', TextType::class, array(
                'attr'      => array(
                    'class' => 'form-control'
                )
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Shann\UserBundle\Entity\Experience'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'shann_userbundle_experience';
    }


}
