<?php

namespace Shann\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

class UserRegistrationForm extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, array(
                'attr'          => array(
                    'class'     => 'form-control',
                    'placeholder'   => 'Firstname'
                ),
                'label_attr'    => array(
                    'class'     => 'control-label'
                ),
            ))
            ->add('lastname', TextType::class, array(
                'attr'          => array(
                    'class'     => 'form-control',
                    'placeholder'   => 'Lastname'
                ),
                'label_attr'    => array(
                    'class'     => 'control-label'
                )
            ))
            ->add('email', EmailType::class, array(
                'attr'          => array(
                    'class'     => 'form-control',
                    'placeholder'   => 'Email'
                ),
                'label_attr'    => array(
                    'class'     => 'control-label',
                    'placeholder'   => 'Firstname'
                )
            ))
            ->add('plainPassword', RepeatedType::class, array(
                'type'          => PasswordType::class,
                'options'       => array(
                    'attr'              => array(
                        'class'         => 'form-control',
                        'placeholder'   => 'Password'
                    ),
                    'label'     => 'Password'
                ),
                'second_options'       => array(
                    'attr'              => array(
                        'class'         => 'form-control',
                        'placeholder'   => 'Confirm Password'
                    ),
                    'label'     => 'Repeat Password'
                ),
                'label_attr'    => array(
                    'class'     => 'control-label'
                )
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'        => 'Shann\UserBundle\Entity\User'
        ));
    }

}
