<?php

namespace Shann\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class LoginForm extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('_username', TextType::class, array(
                'attr'              => array(
                    'class'         => 'form-control input-no-border',
                    'placeholder'   => 'Username'
                ),
                'label_attr'        => array(
                    'class'         => 'control-label'
                )
            ))
            ->add('_password', PasswordType::class, array(
                'attr'              => array(
                    'class'         => 'form-control input-no-border',
                    'placeholder'   => 'Password'
                ),
                'label_attr'        => array(
                    'class'         => 'control-label'
                )
            ))
        ;
    }

}
