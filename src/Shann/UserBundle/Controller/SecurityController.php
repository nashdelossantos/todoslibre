<?php

namespace Shann\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

use Shann\UserBundle\Entity\User;
use Shann\UserBundle\Form\LoginForm;
use Shann\UserBundle\Form\UserRegistrationForm;

class SecurityController extends Controller
{
	/**
	 * [loginAction description]
	 * @return [type] [description]
	 */
	public function loginAction()
	{
		$authenticationUtils = $this->get('security.authentication_utils');

	    // get the login error if there is one
	    $error = $authenticationUtils->getLastAuthenticationError();

	    // last username entered by the user
	    $lastUsername = $authenticationUtils->getLastUsername();

	    $form = $this->createForm(LoginForm::class, [
	    	'_username'	=> $lastUsername
	    ]);

	    return $this->render('ShannUserBundle:Auth:login.html.twig', array(
	        'last_username' => $lastUsername,
	        'error'         => $error,
	        'form'			=> $form->createView(),
	    ));
	}

	/**
	 * [logoutAction description]
	 * @return [type] [description]
	 */
	public function logoutAction()
	{
		throw new \Exception('this should not be reached!');
	}

	/**
	 * [registerAction description]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function registerAction(Request $request)
    {
    	$user = new User();

    	$form = $this->createForm(UserRegistrationForm::class, $user);
    	$form->handleRequest($request);

    	if ($form->isValid()) {
    		$em = $this->getDoctrine()->getManager();

    		$em->persist($user);
    		$em->flush();

    		$this->get('session')
    			 ->getFlashBag()
    			 ->add('success', 'Thank you for registering!');

    		return $this->get('security.authentication.guard_handler')
                ->authenticateUserAndHandleSuccess(
                    $user,
                    $request,
                    $this->get('app.security.login_form_authenticator'),
                    'main'
                );
    		//return $this->redirectToRoute('Shann_front_homepage');
    	}

    	return $this->render('ShannUserBundle:Auth:register.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
