<?php

namespace Shann\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Shann\UserBundle\Entity\User;

class DefaultController extends Controller
{
    private $route = 'dashboard';

    /**
     * [dashboardAction description]
     * @return [type] [description]
     */
    public function dashboardAction()
    {
        $user = $this->getUser();
        if (!$user instanceof User) {
            return $this->redirectToRoute('security_login');
        }

    	$em = $this->getDoctrine()->getManager();

    	return $this->render('ShannUserBundle:Front/Account:dashboard.html.twig', array(
            'route'     => $this->route,
    	));
    }
}
