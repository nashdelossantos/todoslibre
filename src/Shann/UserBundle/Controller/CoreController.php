<?php

namespace Shann\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

use Shann\UserBundle\Entity\User;

class CoreController extends Controller
{
	/**
	 * Check if user is auth
	 *
	 * @return boolean [description]
	 */
	public function isAuth()
	{
		$user = $this->getUser();
		if (!$user instanceof User) {
			return false;
		}

		return true;
	}

	public function em()
	{
		return $this->getDoctrine()->getManager();
	}

	public function route(Request $request)
	{
		return $request->get('_route');
	}

}
