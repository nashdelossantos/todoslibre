<?php

namespace Shann\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Responsibility
 *
 * @ORM\Table(name="responsibility")
 * @ORM\Entity(repositoryClass="Shann\UserBundle\Repository\ResponsibilityRepository")
 */
class Responsibility
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Experience")
     * @ORM\JoinColumn(name="experience", referencedColumnName="id", onDelete="SET NULL")
     */
    private $experience;

    /**
     * @ORM\Column(name="detail", type="text")
     */
    private $detail;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set experience
     *
     * @param \Shann\UserBundle\Entity\Experience $experience
     *
     * @return Responsibility
     */
    public function setExperience(\Shann\UserBundle\Entity\Experience $experience = null)
    {
        $this->experience = $experience;

        return $this;
    }

    /**
     * Get experience
     *
     * @return \Shann\UserBundle\Entity\Experience
     */
    public function getExperience()
    {
        return $this->experience;
    }

    /**
     * Set detail
     *
     * @param string $detail
     *
     * @return Responsibility
     */
    public function setDetail($detail)
    {
        $this->detail = $detail;

        return $this;
    }

    /**
     * Get detail
     *
     * @return string
     */
    public function getDetail()
    {
        return $this->detail;
    }
}
