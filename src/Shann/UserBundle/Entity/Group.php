<?php

namespace Shann\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * User
 *
 * @ORM\Table(name="groups")
 * @ORM\Entity(repositoryClass="Shann\UserBundle\Repository\GroupRepository")
 */
class Group extends Controller
{
	/**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @ORM\ManyToMany(targetEntity="User", mappedBy="groups")
     */
    private $users;

    /**
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(name="slug", type="string", length=60)
     */
    private $slug;

    /**
     * @ORM\Column(name="initial", type="string", length=6)
     */
    private $initial;

   	/**
   	 * @ORM\Column(name="role", type="string", length=60)
   	 */
   	private $role;

    /**
     * @ORM\Column(name="permissions", type="text", nullable=true)
     */
    private $permissions;

    /**
    * Get id
    * @return
    */
    public function getId()
    {
        return $this->id;
    }

    /**
    * Set id
    * @return $this
    */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
    * Get name
    * @return
    */
    public function getName()
    {
        return $this->name;
    }

    /**
    * Set name
    * @return $this
    */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
    * Get role
    * @return
    */
    public function getRole()
    {
        return $this->role;
    }

    /**
    * Set role
    * @return $this
    */
    public function setRole($role)
    {
        $this->role = $role;
        return $this;
    }

    /**
    * Get slug
    * @return
    */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
    * Set slug
    * @return $this
    */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * Set initial
     *
     * @param string $initial
     *
     * @return Group
     */
    public function setInitial($initial)
    {
        $this->initial = $initial;

        return $this;
    }

    /**
     * Get initial
     *
     * @return string
     */
    public function getInitial()
    {
        return $this->initial;
    }

    /**
     * Set permissions
     *
     * @param string $permissions
     *
     * @return Group
     */
    public function setPermissions($permissions)
    {
        $this->permissions = $permissions;

        return $this;
    }

    /**
     * Get permissions
     *
     * @return string
     */
    public function getPermissions()
    {
        return $this->permissions;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add user
     *
     * @param \Shann\UserBundle\Entity\User $user
     *
     * @return Group
     */
    public function addUser(\Shann\UserBundle\Entity\User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \Shann\UserBundle\Entity\User $user
     */
    public function removeUser(\Shann\UserBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }
}
