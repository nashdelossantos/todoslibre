<?php

namespace Shann\MessageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * User
 *
 * @ORM\Table(name="thread")
 * @ORM\Entity(repositoryClass="Shann\MessageBundle\Repository\ThreadRepository")
 */
class Thread
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Shann\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Shann\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="recipient", referencedColumnName="id")
     */
    private $recipient;

    /**
     * @ORM\OneToMany(targetEntity="Message", mappedBy="thread")
     */
    private $messages;

    /**
     * @ORM\OneToOne(targetEntity="Shann\ItemBundle\Entity\Item")
     * @ORM\JoinColumn(name="item", referencedColumnName="id", nullable=true)
     */
    private $item;

    /**
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->messages = new \Doctrine\Common\Collections\ArrayCollection();
        $this->date     = new \DateTime();
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Thread
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set user
     *
     * @param \Shann\UserBundle\Entity\User $user
     *
     * @return Thread
     */
    public function setUser(\Shann\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Shann\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set recipient
     *
     * @param \Shann\UserBundle\Entity\User $recipient
     *
     * @return Thread
     */
    public function setRecipient(\Shann\UserBundle\Entity\User $recipient = null)
    {
        $this->recipient = $recipient;

        return $this;
    }

    /**
     * Get recipient
     *
     * @return \Shann\UserBundle\Entity\User
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * Add message
     *
     * @param \Shann\MessageBundle\Entity\Message $message
     *
     * @return Thread
     */
    public function addMessage(\Shann\MessageBundle\Entity\Message $message)
    {
        $this->messages[] = $message;

        return $this;
    }

    /**
     * Remove message
     *
     * @param \Shann\MessageBundle\Entity\Message $message
     */
    public function removeMessage(\Shann\MessageBundle\Entity\Message $message)
    {
        $this->messages->removeElement($message);
    }

    /**
     * Get messages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * Set item
     *
     * @param \Shann\ItemBundle\Entity\Item $item
     *
     * @return Thread
     */
    public function setItem(\Shann\ItemBundle\Entity\Item $item = null)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item
     *
     * @return \Shann\ItemBundle\Entity\Item
     */
    public function getItem()
    {
        return $this->item;
    }
}
