<?php

namespace Shann\MessageBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

use Shann\MessageBundle\Entity\Thread;
use Shann\MessageBundle\Entity\Message;

class MessageController extends Controller
{
    public function newAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
        	return new JsonResponse(array(
        		'error' => true,
        		'title'	=> 'Method Not Allowed!',
        		'text'	=> 'Please contact the admin.'
        	));
        }

        $user = $this->getUser();
        if (!$user->hasPermission('message_can_create')) {
            return new JsonResponse(array(
        		'error' => true,
        		'title'	=> 'Unauthorised Access!',
        		'text'	=> 'You don\'t have enough permission.'
        	));
        }

        if (!$recipientId = $request->request->get('recipientid')) {
        	return new JsonResponse(array(
        		'error' => true,
        		'title'	=> 'User Error!',
        		'text'	=> 'There is an error with the user when sendint a message.'
        	));
        }

        $em = $this->getDoctrine()->getManager();
        if (!$recipient = $em->getRepository('ShannUserBundle:User')->findOneById($recipientId)) {
        	return new JsonResponse(array(
        		'error' => true,
        		'title'	=> 'Recipient Not Foud!',
        		'text'	=> 'There is an error with the recipient of the message.'
        	));
        }

        if (!$itemId = $request->request->get('itemid')) {
        	return new JsonResponse(array(
        		'error' => true,
        		'title'	=> 'Item Error!',
        		'text'	=> 'There is an error with the item when sending a message to the user.'
        	));
        }

        if (!$item = $em->getRepository('ShannItemBundle:Item')->findOneById($itemId)) {
        	return new JsonResponse(array(
        		'error' => true,
        		'title'	=> 'Item Not Found!',
        		'text'	=> 'There is an error with the item or is missing'
        	));
        }

        $thread 	= new Thread();
        $message 	= new Message();

        $thread
        	->setUser($user)
        	->setRecipient($recipient)
        	->addMessage($message)
        	->setItem($item)
        ;

        $message
        	->setThread($thread)
        	->setContent($request->request->get('msg'))
        ;

        $em->persist($message);
        $em->persist($thread);
        $em->flush();

        return new JsonResponse(array(
    		'success' => true,
    		'title'	=> 'Message Sent!',
    		'text'	=> 'Your message to '.$recipient->getFirstname() . ' has been successfully sent!'
    	));

    }
}
