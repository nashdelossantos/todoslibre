<?php

namespace Shann\ItemBundle\Controller\User;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;

use Intervention\Image\ImageManagerStatic as Image;

use Shann\UserBundle\Entity\User;
use Shann\ItemBundle\Entity\Item;
use Shann\ItemBundle\Entity\ItemImage;
use Shann\ItemBundle\Form\ItemType;

class ItemController extends \Shann\UserBundle\Controller\CoreController
{
    const PARENT_ROUTE = 'item';

    /**
     * List of active items
     *
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function indexAction(Request $request)
    {
        if (!$this->isAuth()) {
    		throw new Exception("Access Denied!", 1);
    	}

        $user = $this->getUser();
        $filePaths = $this->getParameter('filepaths');
        Item::setPaths($filePaths, $user->getId());

    	$items = $this->em()->getRepository('ShannItemBundle:Item')->findBy(
    		array('user' => $this->getUser())
    	);

        return $this->render('ShannItemBundle:User\Item:index.html.twig', array(
        	'title'		       => 'Items',
            'route'            => [
        	   'parentRoute'      => self::PARENT_ROUTE,
                'childRoute'       => $this->route($request),
            ],
        	'items'		       => $items,
        ));
    }

    /**
     * List of received items
     *
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function receivedAction(Request $request)
    {
        if (!$this->isAuth()) {
            throw new Exception("Access Denied!", 1);
        }

        $items = $this->em()->getRepository('ShannItemBundle:Item')->findBy(
            array('user' => $this->getUser())
        );

        $route = $request->get('_route');
        return $this->render('ShannItemBundle:User\Item:received.html.twig', array(
            'title'            => 'Items',
            'route'            => [
               'parentRoute'      => self::PARENT_ROUTE,
                'childRoute'       => $this->route($request),
            ],
            'items'            => $items,
        ));
    }

    /**
     * List of Given items
     *
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function givenAction(Request $request)
    {
        if (!$this->isAuth()) {
            throw new Exception("Access Denied!", 1);
        }

        $items = $this->em()->getRepository('ShannItemBundle:Item')->findBy(
            array('user' => $this->getUser())
        );

        return $this->render('ShannItemBundle:User\Item:given.html.twig', array(
            'title'            => 'Items',
            'route'            => [
               'parentRoute'      => self::PARENT_ROUTE,
                'childRoute'       => $this->route($request),
            ],
            'items'            => $items,
        ));
    }

    /**
     * New Item
     *
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function newAction(Request $request)
    {
        if (!$this->isAuth()) {
            throw new Exception("Access Denied!", 1);
        }
        $user = $this->getUser();

        $filePaths = $this->getParameter('filepaths');
        $webPath = $filePaths['web'];
        $itemPath = $webPath . $filePaths['item_image'] . $this->getUser()->getId() . '/';

        Item::setPaths($filePaths, $user->getId());

        $item = new Item();

        $form = $this->createForm(ItemType::class, $item);

        $form->handleRequest($request);

        if ($form->isValid()) {

            $fs = new Filesystem();


            if ($files = $request->files->get('shann_itembundle_item')['photos']) {
                $validFilelist = explode(',', $request->request->get('bucketlist'));

                if (!$fs->exists($itemPath)) {
                    try {
                        $fs->mkdir($itemPath);
                    } catch (IOExceptionInterface $e) {
                        throw new Exception("An error occurred while creating your directory at ".$e->getPath(), 1);
                    }
                }

                $em = $this->em();

                foreach ($files as $file) {
                    if ($file === null) {
                        continue;
                    }

                    $originalName = $file->getClientOriginalName();
                    if (!in_array($originalName, $validFilelist)) {
                        continue;
                    }

                    $image          = Image::make($file);
                    $filename       = md5(uniqid());
                    $ext            = '.jpg';
                    $fullFilename   = $filename . $ext;

                    $image->resize(900, null, function($constraint) {
                        $constraint->aspectRatio();
                    });

                    $image->save($itemPath . $filename . $ext, 50);

                    $image->resize(500, null, function($constraint) {
                        $constraint->aspectRatio();
                    });
                    $image->crop(250,250);

                    $thumbName = $filename . '-thumb' . $ext;
                    $image->save($itemPath . $thumbName, 50);

                    $itemImage  = new ItemImage();
                    $itemImage
                        ->setOriginalName($originalName)
                        ->setFilename($fullFilename)
                        ->setThumb($thumbName)
                        ->setItem($item)
                        ->setUser($user);

                    $em->persist($itemImage);

                    $user->addItem($item)
                         ->addItemImage($itemImage);

                    $item->addImage($itemImage);

                }
            }

            $item->setUser($user);

            $em->persist($item);
            $em->persist($user);

            $em->flush();

            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Item successfully saved!');

            return $this->redirectToRoute('item_index');
        }

        return $this->render('ShannItemBundle:User\Item:form.html.twig', array(
            'form'      => $form->createView(),
            'title'            => 'Items',
            'route'            => [
               'parentRoute'      => self::PARENT_ROUTE,
                'childRoute'       => $this->route($request),
            ],
        ));
    }

    /**
     * Edit an item
     * @param  Request $request [description]
     * @param  integer $id      [description]
     * @return [type]           [description]
     */
    public function editAction(Request $request, $id = 0)
    {
        if (!$id) {
            throw new Exception("Item ID Invalid!", 1);
        }

        if (!$this->isAuth()) {
            throw new Exception("Access Denied!", 1);
        }

        $filePaths = $this->getParameter('filepaths');
        $webPath = $filePaths['web'];
        $itemPath = $webPath . $filePaths['item_image'] . $this->getUser()->getId() . '/';

        $user = $this->getUser();
        Item::setPaths($filePaths, $user->getId());

        if (!$item = $this->em()->getRepository('ShannItemBundle:Item')->findOneBy(array(
           'id'     => $id,
           'user'   => $user,
        ))) {
            throw new Exception("Item Not Found!", 1);
        }

        $form = $this->createForm(ItemType::class, $item);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->em();

            $em->persist($item);
            $em->flush();

            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Item successfully updated!');

            return $this->redirectToRoute('item_index');
        }

        return $this->render('ShannItemBundle:User\Item:form.html.twig', array(
            'title'     => 'Editing an Item',
            'form'      => $form->createView(),
            'item'      => $item,
            'route'            => [
               'parentRoute'      => self::PARENT_ROUTE,
                'childRoute'      => $this->route($request),
            ],
        ));
    }

    public function deleteAction(Request $request)
    {
        if (!$this->isAuth()) {
            throw new Exception("Access Denied!", 1);
        }

        if (!$request->isXmlHttpRequest()) {
            throw new Exception("Method Not Allowed!", 1);

        }

        if (!$id = $request->request->get('id')) {
            return new JsonResponse(
                array(
                    'error'     => true,
                    'title'     => 'ID Not Found!',
                    'text'      => 'Item ID is unknown or not found!'
                )
            );
        }

        $filePaths = $this->getParameter('filepaths');
        $webPath = $filePaths['web'];
        $itemPath = $webPath . $filePaths['item_image'] . $this->getUser()->getId() . '/';

        $user = $this->getUser();
        Item::setPaths($filePaths, $user->getId());

        $em = $this->em();

        if (!$item = $em->getRepository('ShannItemBundle:Item')->findOneBy(
            array(
                'id'    => $id,
                'user'  => $user,
            )
        )) {
            return new JsonResponse(
                array(
                    'error'     => true,
                    'title'     => 'Item Not Found!',
                    'text'      => 'Item is unknown or not found!'
                )
            );
        }

        $fs = new Filesystem();
        foreach ($item->getImages() as $image) {
            $actualFile = 'uploads/' . $user->getId() . '/' . $image->getFilename();
            $thumbFile  = 'uploads/' . $user->getId() . '/' . $image->getThumb();
            if ($fs->exists($actualFile)) {
                $fs->remove($actualFile);
                $fs->remove($thumbFile);
            }
        }

        $em->remove($item);
        $em->flush();

        return new JsonResponse(array('success' => true));
    }
}
