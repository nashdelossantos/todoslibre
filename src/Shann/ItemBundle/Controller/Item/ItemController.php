<?php

/**
 * Display Public Items
 */
namespace Shann\ItemBundle\Controller\Item;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;

use Intervention\Image\ImageManagerStatic as Image;

use Shann\UserBundle\Entity\User;
use Shann\ItemBundle\Entity\Item;
use Shann\ItemBundle\Entity\ItemImage;
use Shann\ItemBundle\Form\ItemType;

class ItemController extends \Shann\UserBundle\Controller\CoreController
{
    const PARENT_ROUTE = 'browse';

    public function browseAction(Request $request)
    {
        
        $em = $this->em();
        
        $items = $em->getRepository('ShannItemBundle:Item')->findLatest();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $items, 
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('ShannItemBundle:Item:index.html.twig', array(
            'items'             => $items,
            'title'             => 'Items',
            'pagination'        => $pagination,
            'route'             => [
               'parentRoute'    => self::PARENT_ROUTE,
                'childRoute'    => $this->route($request),
            ],
        ));
    }
}
