<?php

namespace Shann\ItemBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ItemImage
 *
 * @ORM\Table(name="item_image")
 * @ORM\Entity(repositoryClass="Shann\ItemBundle\Repository\ItemImageRepository")
 */
class ItemImage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="original_name", type="string", length=255, nullable=true)
     */
    private $originalName;

    /**
     * @ORM\Column(name="filename", type="string", length=255)
     */
    private $filename;

    /**
     * @ORM\Column(name="thumb", type="string", length=255)
     */
    private $thumb;

    /**
     * @ORM\ManyToOne(targetEntity="Item")
     * @ORM\JoinColumn(name="item", referencedColumnName="id")
     */
    private $item;

    /**
     * @ORM\ManyToOne(targetEntity="Shann\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user", referencedColumnName="id")
     */
    private $user;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set item
     *
     * @param \Shann\ItemBundle\Entity\Item $item
     *
     * @return ItemImage
     */
    public function setItem(\Shann\ItemBundle\Entity\Item $item = null)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item
     *
     * @return \Shann\ItemBundle\Entity\Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Set user
     *
     * @param \Shann\UserBundle\Entity\User $user
     *
     * @return ItemImage
     */
    public function setUser(\Shann\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Shann\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set filename
     *
     * @param string $filename
     *
     * @return ItemImage
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set thumb
     *
     * @param string $thumb
     *
     * @return ItemImage
     */
    public function setThumb($thumb)
    {
        $this->thumb = $thumb;

        return $this;
    }

    /**
     * Get thumb
     *
     * @return string
     */
    public function getThumb()
    {
        return $this->thumb;
    }

    /**
     * Set originalName
     *
     * @param string $originalName
     *
     * @return ItemImage
     */
    public function setOriginalName($originalName)
    {
        $this->originalName = $originalName;

        return $this;
    }

    /**
     * Get originalName
     *
     * @return string
     */
    public function getOriginalName()
    {
        return $this->originalName;
    }

    public function getImageThumb()
    {
        if ($this->thumb !== null) {
            return '/uploads/' . $this->getUser()->getId() . '/' . $this->thumb;
        }

        return null;
    }

    public function getWideImage()
    {
        if ($this->filename !== null) {
            return '/uploads/' . $this->getUser()->getId() . '/' . $this->filename;
        }
        
        return null;
    }
}
