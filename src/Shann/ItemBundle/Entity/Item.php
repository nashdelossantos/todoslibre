<?php

namespace Shann\ItemBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Item
 *
 * @ORM\Table(name="item")
 * @ORM\Entity(repositoryClass="Shann\ItemBundle\Repository\ItemRepository")
 */
class Item
{
    const DEFAULT_IMAGE = 'http://via.placeholder.com/250x250?text=No+image';

    const STATUS_ACTIVE     = 1;
    const STATUS_DRAFT      = 2;
    const STATUS_RECEIVED   = 3;
    const STATUS_GIVEN      = 4;

    public function __construct()
    {
        $this->createdAt = new \Datetime();
    }

    public static function getStatusNames()
    {
        return [
            self::STATUS_ACTIVE => 'Active',
            self::STATUS_DRAFT  => 'Draft',
        ];
    }

    public function getStatusName($id = 0)
    {
        if (intval($id)) {
            return self::getStatusNames()[$id];
        }
        return null;
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="ItemImage", mappedBy="item", cascade={"persist", "remove"})
     */
    private $images;

    /**
     * @ORM\Column(name="status", type="integer")
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="Shann\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="recipient", referencedColumnName="id")
     */
    private $recipient;

    /**
     * @ORM\ManyToOne(targetEntity="Shann\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="date_given", type="datetime", nullable=true)
     */
    private $dateGiven;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Item
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Item
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Item
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set recipient
     *
     * @param \Shann\UserBundle\Entity\User $recipient
     *
     * @return Item
     */
    public function setRecipient(\Shann\UserBundle\Entity\User $recipient = null)
    {
        $this->recipient = $recipient;

        return $this;
    }

    /**
     * Get recipient
     *
     * @return \Shann\UserBundle\Entity\User
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * Set user
     *
     * @param \Shann\UserBundle\Entity\User $user
     *
     * @return Item
     */
    public function setUser(\Shann\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Shann\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add image
     *
     * @param \Shann\ItemBundle\Entity\ItemImage $image
     *
     * @return Item
     */
    public function addImage(\Shann\ItemBundle\Entity\ItemImage $image)
    {
        $this->images[] = $image;

        return $this;
    }

    /**
     * Remove image
     *
     * @param \Shann\ItemBundle\Entity\ItemImage $image
     */
    public function removeImage(\Shann\ItemBundle\Entity\ItemImage $image)
    {
        $this->images->removeElement($image);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages()
    {
        return $this->images;
    }


    static $imagePath = null;

    public static function setPaths($filepaths, $userId)
    {
        if ($filepaths) {
            self::$imagePath =  $filepaths['item_image'] . $userId;
        }
    }

    public function getImageThumb()
    {
        if (count($this->images) > 0) {
            return self::$imagePath . '/' . $this->images->first()->getThumb();
        }
       return null;
    }

    public function imagePath()
    {
        if (count($this->images) > 0) {
            return self::$imagePath . '/';
        }
        return null;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Item
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set dateGiven
     *
     * @param \DateTime $dateGiven
     *
     * @return Item
     */
    public function setDateGiven($dateGiven)
    {
        $this->dateGiven = $dateGiven;

        return $this;
    }

    /**
     * Get dateGiven
     *
     * @return \DateTime
     */
    public function getDateGiven()
    {
        return $this->dateGiven;
    }
}
