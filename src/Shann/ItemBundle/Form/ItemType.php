<?php

namespace Shann\ItemBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

use Shann\ItemBundle\Entity\Item;

class ItemType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'label'         => 'Item Name',
                'label_attr'    => array(
                    'class'     => 'control-label'
                ),
                'attr'          => array(
                    'placeholder'   => 'Enter item name',
                    'class'     => 'form-control',
                    'type'      => 'text',
                    'required'  => true
                )
            ))
            ->add('description', TextareaType::class, array(
                'label'         => 'Item Description',
                'attr'          => array(
                    'class'     => 'form-control',
                    'rows'      => '10'
                )
            ))
            ->add('photos', FileType::class, array(
                'label'         => false,
                'required'      => false,
                'mapped'        => false,
                'attr'          => array(
                    'class'     => 'hidden'
                ),
                'multiple'      => true
            ))
            ->add('status', ChoiceType::class, array(
                'multiple'      => false,
                'expanded'      => false,
                'label'         => 'Item Status',
                'label_attr'    => array(
                    'class'     => 'control-label'
                ),
                'choices'       => array_flip(Item::getStatusNames()),
                'attr'          => array(
                    'data-style'=> 'btn btn-block btn-block',
                    'class'     => 'selectpicker',
                )
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Shann\ItemBundle\Entity\Item'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'shann_itembundle_item';
    }


}
